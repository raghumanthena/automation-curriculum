package library;



import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import accelerators.ActionDriver;
import accelerators.ReadProperties;
import locators.Homepage;
import reporting.Reporting;

public class BusinessFunctions extends ActionDriver{
	
	//private static final String[] li= {"All","Program","Specialisation","Course"};
	List valid=Arrays.asList("All","Program","Specialisation","Course");
	ReadProperties rp = new ReadProperties();
	String url  =  rp.readProperties().getProperty("url");
	String username=rp.readProperties().getProperty("username");
	String password= rp.readProperties().getProperty("password");

	/**
	 * Launch South East WebSite
	 */
	public void launchHandBook() {
		launchUrl(url);
	}
	

	public void login() throws HeadlessException, IOException, AWTException
	{
		try {
			//click(locators.Homepage.USERNAME, "Username");
			enter(Homepage.USERNAME, username, "Username");
			enter(Homepage.PASSWORD, password, "Password");
			click(Homepage.LOGIN_BTN, "Login button in homepage");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Reporting.logResults("Pass", "Login Functionality", "Login to curriculum was succesfull");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Reporting.logResults("Fail", "Login Functionality", "Unable to login to curriculum");
		}
	}

	public boolean verify_Home_Page() throws Throwable
	{
		boolean flag=false;
		
		try {
			String title=getTitle();
			Thread.sleep(2000);
			 flag=assertTitle(title);
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify title assertion", "Assertion of title failed");
			}
			else
			{
				Reporting.logResults("pass", "Verify title assertion", "Assertion of title passed");
			}
			
			flag =isElementPresent(locators.Homepage.logo, " Home page Logo");
			if(!flag)
			{
				Reporting.logResults("Fail", "Element visibility in the page", "Element not visible in the page");
				return flag;
			}
			isElementPresent(locators.Homepage.recent," Recent Button");
			
			isElementPresent(locators.Homepage.browse," Browse Element");
			
			isElementPresent(locators.Homepage.areaofInterest,"Areas of Interest");
			
			isElementPresent(locators.Homepage.faculties,"Faculties");
			
			
			return flag;
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return flag;
	}
	
	public void verify_Search_Page_bySearchButton(String text) throws Throwable
	{


		boolean flag=false;
		try
		{
			flag = click(locators.Homepage.search,"search button");
			Thread.sleep(2000);
			if(!flag)
			{
				Reporting.logResults("fail", "click action", "click failed");
				return;
			}

			flag =enter(locators.Homepage.search, text, "Search button in home page");
			Thread.sleep(2000);
			if(!flag)
			{
				Reporting.logResults("fail", "enter action", "enter failed");
			}

			flag=click(locators.Homepage.searchButton, "Search Button");
			if(!flag)
			{
				Reporting.logResults("fail", "Search button click action", "Search button was not clicked");
			}

			Thread.sleep(3000);

			flag=assertTitle("Search");
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify title assertion", "Assertion of title failed");
				
			}
			else
			{
				Reporting.logResults("pass", "Verify title assertion", "Assertion of title passed");
				
			}
			
			String value=driver.findElement(By.xpath("//input[@class='m-advanced-search-field__input']")).getAttribute("value");
			
			flag=value.contains(text);
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify search text in search page", "Entered text in home page search and text in search page is different");
			}
			else
			{
				Reporting.logResults("pass", "Verify search text in search page", "Entered text in home page search and text in search page matches");
			}
			
			String val=driver.findElement(By.xpath("//li[@class='m-advanced-results-total']/span")).getText();
			flag=val.contains(text);
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify search results in search page", "Entered text in home page search and text in search results are different");
			}
			else
			{
				Reporting.logResults("pass", "Verify search results in search page", "Entered text in home page search and text in search results are matches");
			}
			
			searchPageValidation();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	
	public boolean searchPageValidation() throws HeadlessException, IOException, AWTException
	
	{
		boolean flag=false;
	
		for (int i = 1; i <= valid.size(); i++) {
			
			String valu=driver.findElement(By.xpath("//ul[@class='m-tab-panel']/li["+i+"]/button")).getText();
			
			if(valid.contains(valu))
			{
				Reporting.logResults("pass", "Verify search filters  All,Program,Specialisation and Course are present in page", " Filters are present in the page");
			}
			else
				Reporting.logResults("fail", "Verify search filters  All,Program,Specialisation and Course are present in page", "Filters are not present in the page");
			flag=false;
			
			
		}
		
		return flag;
		
	}
	
	//   //span[text()='See all results']
	
	public void searchBySeeAllResults(String text) throws Throwable
	{
		boolean flag=false;
		try
		{

			flag = click(locators.Homepage.search,"search button");
			Thread.sleep(2000);
			if(!flag)
			{
				Reporting.logResults("fail", "click action", "click failed");
				return;
			}

			flag =enter(locators.Homepage.search, text, "Search button in home page");
			Thread.sleep(2000);
			if(!flag)
			{
				Reporting.logResults("fail", "enter action", "enter failed");
			}

			flag=click(locators.Homepage.searchByAllResults, "Search by all results");
			if(!flag)
			{
				Reporting.logResults("fail", "Search by all results  click action", "Search by all results was failed");
			}

			Thread.sleep(3000);

			flag=assertTitle("Search");
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify title assertion", "Assertion of title failed");
				
			}
			else
			{
				Reporting.logResults("pass", "Verify title assertion", "Assertion of title passed");
				
			}
			
			String value=driver.findElement(By.xpath("//input[@class='m-advanced-search-field__input']")).getAttribute("value");
			
			flag=value.contains(text);
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify search text in search page", "Entered text in home page search and text in search page is different");
			}
			else
			{
				Reporting.logResults("pass", "Verify search text in search page", "Entered text in home page search and text in search page matches");
			}
			
			String val=driver.findElement(By.xpath("//li[@class='m-advanced-results-total']/span")).getText();
			flag=val.contains(text);
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify search results in search page", "Entered text in home page search and text in search results are different");
			}
			else
			{
				Reporting.logResults("pass", "Verify search results in search page", "Entered text in home page search and text in search results are matches");
			}
			
			searchPageValidation();
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void advanceSearch(String text) throws Throwable
	{

		boolean flag=false;
		try
		{

			flag = click(locators.Homepage.search,"search button");
			Thread.sleep(2000);
			if(!flag)
			{
				Reporting.logResults("fail", "click action", "click failed");
				return;
			}

			flag =enter(locators.Homepage.search, text, "Search button in home page");
			Thread.sleep(2000);
			if(!flag)
			{
				Reporting.logResults("fail", "enter action", "enter failed");
			}

			flag=click(locators.Homepage.ADVANCEDSEARCH, "Search by all results");
			if(!flag)
			{
				Reporting.logResults("fail", "Search by all results  click action", "Search by all results was failed");
			}

			Thread.sleep(3000);

			flag=assertTitle("Search");
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify title assertion", "Assertion of title failed");
				
			}
			else
			{
				Reporting.logResults("pass", "Verify title assertion", "Assertion of title passed");
				
			}
			
			String value=driver.findElement(By.xpath("//input[@class='m-advanced-search-field__input']")).getAttribute("value");
			
			flag=value.contains(text);
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify search text in search page", "Entered text in home page search and text in search page is different");
			}
			else
			{
				Reporting.logResults("pass", "Verify search text in search page", "Entered text in home page search and text in search page matches");
			}
			
			String val=driver.findElement(By.xpath("//li[@class='m-advanced-results-total']/span")).getText();
			flag=val.contains(text);
			if(!flag)
			{
				Reporting.logResults("Fail", "Verify search results in search page", "Entered text in home page search and text in search results are different");
			}
			else
			{
				Reporting.logResults("pass", "Verify search results in search page", "Entered text in home page search and text in search results are matches");
			}
			
			searchPageValidation();
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
	}
	
}
