package locators;

import org.openqa.selenium.By;

public class Homepage {

/*	public static By username = By.xpath("//input[@class='_2zrpKA']");
	public static By password = By.xpath("//input[@class='_2zrpKA _3v41xv']");
	public static By signin = By.xpath("//button[@type='submit'][@class='_2AkmmA _1LctnI _7UHT_c']");
	public static By electronics = By.xpath("//a[@title='Electronics']");*/
	public static By USERNAME = By.xpath("//input[@name='_loginUserName']");
	public static By PASSWORD=By.xpath("//input[@name='_loginPassword']");
	public static By LOGIN_BTN=By.xpath("//button[@type='submit']");
	public static By search=By.xpath("//input[@class='m-mini-search-field__input']");
	public static By searchButton=By.xpath("//i[text()='search']");
	public static By logo=By.xpath("//*[@class='a-logo']");
	public static By recent=By.xpath("//h3[text()='Recently viewed']");
	public static By browse=By.xpath("//h2[text()='Browse']");
	public static By areaofInterest=By.xpath("//button[text()='By Areas of Interest']");
	public static By faculties=By.xpath("//button[text()='By Faculties']");
	public static By agriculture=By.xpath("//h3[text()='Natural and Physical Sciences']");
	public static By searchByAllResults = By.xpath("//span[text()='See all results']");
	public static By ADVANCEDSEARCH =By.xpath("//span[text()='Advanced search']");
	
}
