package testscripts;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import library.BusinessFunctions;
import reporting.Reporting;

public class Verify_Academia extends BusinessFunctions{

	
	@Test
	public void maijg() throws Throwable
	{
		Reporting.setReportingValues(this.getClass().getName());
		try {
			verify_Search_Page_bySearchButton("poetry");
			count();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public boolean count() throws InterruptedException, HeadlessException, IOException, AWTException
	{
		boolean bstatus=false;


			int cit=0;

		String icount=driver.findElement(By.xpath("//li[@class='m-advanced-results-total']/span")).getText();
		String val=icount.substring(0, 3).trim();
		int cal=Integer.parseInt(val);


		List<WebElement> l=driver.findElements(By.xpath("//ul[@id='advanced-search__suggestions']//following::li"));
		int cItems=l.size();
		
		if(driver.findElement(By.xpath("//span[text()='chevron_right']")).isDisplayed())
		{
			 bstatus=true;
		}
		while(bstatus)
			{
				driver.findElement(By.xpath("//span[text()='chevron_right']")).click();
				Thread.sleep(2000);

				List<WebElement> li=driver.findElements(By.xpath("//ul[@id='advanced-search__suggestions']//following::li"));
				int cItems1=li.size();
				cit=cItems1+cit;
				if(driver.findElement(By.xpath("//span[text()='chevron_right']"))  == null) {
					break;
				}
				
			}
		int total=cit+cItems;
		System.out.println("Total items is :" + total);
		
		if(cal==total)
		{
			Reporting.logResults("Pass", "search count", "passed");
			return bstatus;
		}
		else
		{
			Reporting.logResults("fail", "search count", "failed");
			return false;
		}
	}
	}
	
	

