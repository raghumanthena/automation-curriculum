package testscripts;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;

import org.testng.annotations.Test;

import library.BusinessFunctions;

public class VerifyHomePage extends BusinessFunctions{
	
	@Test
	public void loginPage() throws HeadlessException, IOException, AWTException
	{
		launchHandBook();
		login();
	}

}
