package testscripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import accelerators.ReadProperties;
import library.BusinessFunctions;
import reporting.Reporting;

@Test
public class Area_of_Interst extends BusinessFunctions {
	
	
	
	public void test() throws Throwable
	{
		Reporting.setReportingValues(this.getClass().getName());
		try
		{
			launchHandBook();
			verify_Home_Page();
			interst();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	public boolean interst() throws Throwable
	{
		try 
		{
			jsClick(locators.Homepage.areaofInterest, "area of Interest");
			scrollToView(locators.Homepage.agriculture, "Agriculuture");
			click(locators.Homepage.agriculture, "Agriculutre");
			navigate("Program");
			navigate("Double Degree Program");
			navigate("Specialisation");

			//Navigae to program 
			
			
		}
		catch(Exception e)
		{
			return false;
			
		}
		return true;
	}
	

	
	public boolean navigate(String value) throws InterruptedException
	{
		//driver.findElement(By.xpath("(//div[@class='m-page-nav-list'])[1]/a[contains(text(),'"+value +""')]"));
				driver.findElement(By.xpath("(//div[@class='m-page-nav-list'])[1]/a[contains(text(),'"+value+"')]")).click();
				Thread.sleep(2000);
		return true;
	}
}
