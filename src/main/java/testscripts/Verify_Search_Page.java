package testscripts;

import org.testng.annotations.Test;

import library.BusinessFunctions;
import reporting.Reporting;

@Test
public class Verify_Search_Page extends BusinessFunctions{
	
	
	public void search() throws Throwable
	{
		Reporting.setReportingValues(this.getClass().getName());
		try {
			verify_Search_Page_bySearchButton("Bachelor of Arts");
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	/**
	 * This method will validate the Program,specialisation and course in the search page
	 * @throws Throwable 
	 */
	
	public boolean validate() throws Throwable
	{
		boolean bstatus=false;
		try {
			bstatus=assertTextPresent("All");
			if(!bstatus)
			{
				Reporting.logResults("fail", "Search page assertion", "Assertion faile for all ");
				return false;
			}
			bstatus=assertTextPresent("Program");
			if(!bstatus)
			{
				Reporting.logResults("fail", "Search page assertion", "Assertion faile for all ");
				return false;
			}
			bstatus=assertTextPresent("Specialisation");
			if(!bstatus)
			{
				Reporting.logResults("fail", "Search page assertion", "Assertion failed for all ");
				return false;
			}
			bstatus=assertTextPresent("Course");
			if(!bstatus)
			{
				Reporting.logResults("fail", "Search page assertion", "Assertion faile for all ");
				return false;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return true;
	}
	


}
