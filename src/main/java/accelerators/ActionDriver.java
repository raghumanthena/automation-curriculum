package accelerators;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import reporting.Reporting;




public class ActionDriver extends BaseDriverUtil{

	
	public void launchUrl(String url) {
		
		try {
			driver.get(url);
		}catch(NullPointerException e)
		{
			e.getMessage();
			
		}
	}
	

	/**
	 * Clicks the Element based on the locator provided
	 * Providing locatorName can help identify the name in the reporting
	 * ;
	 * @param locator
	 * @param locatorName
	 * @return
	 *  @author raghu
	 */

	public boolean click(By locator,String locatorName) throws HeadlessException, IOException, AWTException
	{
		boolean flag=false;
		try {
			driver.findElement(locator).click();
			driver.manage().timeouts().implicitlyWait(300,TimeUnit.SECONDS);
			flag=true;
			Reporting.logResults("Pass", "Verify the click element ", "succesfully clicke on" +locatorName);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			Reporting.logResults("Fail", "Verify the click Element", "Unable to click on " +locator);
			return false;
		} 

	}


	
	/**This method will hover over an element mentioned 
	 * @param element
	 * @return
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws HeadlessException 
	 */
	public boolean mouseHover(WebElement element) throws HeadlessException, IOException, AWTException{
		boolean flag=false;
		try{
			Actions a=new Actions(driver);
			a.moveToElement(element).build().perform();
			Thread.sleep(3000);
			flag=true;
			Reporting.logResults("pass", "Hover over an Element", "Sucessfully hovered over an element ");
		}catch(Exception e){
			
			e.printStackTrace();
			Reporting.logResults("Fail", "Hover over an Element", "Failed hovering over an element");
		}
		return flag;
	}

	/**
	 * This method will enter the value in the locator
	 * @param locator
	 * @param value
	 * @param locatorname
	 * @return
	 * @throws HeadlessException
	 * @throws IOException
	 * @throws AWTException
	 */

	public boolean enter(By locator,String value,String locatorname) throws HeadlessException, IOException, AWTException
	{
		boolean  flag=false;
		try {

			driver.findElement(locator).clear();
			driver.findElement(locator).sendKeys(value);
			Reporting.logResults("Pass", "Enter Text", "Succesfully entered text in" +locatorname);
			flag = true;
			return true;

		}
		catch(Exception e)
		{
			Reporting.logResults("Fail", "Enter text ", "Unable to enter text in " +locatorname);
			return false;
		}


	}
	/******************************************************************************************************************************/
	/**
	 * Assert Title of the page.
	 * @param sTitle: Expected title of the page.
	 */
	/******************************************************************************************************************************/



	public static boolean assertTitle(String sTitle) throws Throwable {
		try{
			Assert.assertEquals(getTitle(), sTitle);
			return true;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}


	/**
	 * This Method will return the tile of the page
	 * @return
	 * @throws Throwable
	 */
	public static String getTitle() throws Throwable {

		boolean flag=false;
		String text = driver.getTitle();
		/*if(flag=true){
			Reporting.logResults("Pass", "Get title of page ", "succesfully got title of the page");
		}*/
		return text;
	}

	/**
	 * This method will verify the presence of element in the p
	 * @return
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws HeadlessException 
	 */
	public boolean isElementPresent(By locator,String locatorName) throws HeadlessException, IOException, AWTException{
		boolean flag=false;
		try{
			driver.findElement(locator);
			flag=true;
			Reporting.logResults("Pass", "Verify Element present in the page", "Element"+ locatorName+" is present in the page");
		}catch(Exception e){
			flag=false;
			e.printStackTrace();
			Reporting.logResults("Fail", "Verify Element present in the page", "Element "+ locatorName+" is not visible in the page");
		}
		return flag;

	}
	
	/******************************************************************************************************************************/
	/**
	 * Check the text is presnt or not
	 * @param text: Text wish to assert on the page.
	 */
	/******************************************************************************************************************************/
	public static  boolean assertTextPresent(String sText) throws Throwable {
		boolean flag=false;
		try{
			Assert.assertTrue(isTextPresent(sText));
			flag=true;
		}catch(Exception e){
		}finally{
			if(!flag){
				Reporting.logResults("Fail", "Verify text present in the page", "assertTextPresent"+ sText+" not present in the page");
				return false;
			}else if(flag){
				Reporting.logResults("pass", "Verify text present in the page", "assertTextPresent"+ sText+"  present in the page");
				return true;
			}
		}
		return flag;
	}
	
	/******************************************************************************************************************************/
	/**
	 * Text present or not
	 * 
	 * @param text: Text wish to verify on current page 
	 * 
	 * @return: boolean value(true: if Text present, false: if text not present)
	 */
	/******************************************************************************************************************************/
	public static boolean  isTextPresent(String text) throws Throwable {
		boolean value = driver.getPageSource().contains(text);

		boolean flag=false;
		if (!value) {
			Reporting.logResults("fail", "Verify text present", text +"not present in the page");
			return false;
		} else if(flag) {
			Reporting.logResults("pass", "Verify text present", text +" present in the page");
			return true;
		}
		return value;
	}
	
	
	/************************************************************************************************************************************/
	/**
	 * @param objLocator: Action to be performed on element (Get it from Object repository)
	 * @param sLocatorName: Meaningful name to the element (Ex:Login Button, SignIn Link etc..)
	 * @return --boolean (true or false)
	 * @throws Throwable 
	 */
	/*****************************************************************************************************************************/
	public static boolean jsClick(By objLocator, String sLocatorName)
			throws Throwable {
		boolean flag = false;
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", driver.findElement(objLocator));
			flag = true;

			Reporting.logResults("pass", "java script executor click", "successfully to click on "+sLocatorName);
			return true;
		} catch (Exception e) {
			Reporting.logResults("Fail", "java script executor click", "Failed to click on "+sLocatorName);
			return false;
		}
	}
	
	/************************************************************************************************************************************/
/**
 * 
 * @param locator Action to be performed on element (Get it from Object repository)
 * @param locatorName Meaningful name to the element (Ex:Login Button, SignIn Link etc..)
 * @return boolean (true or false)
 */
  /***************************************************************************************************************************************/

	public static boolean scrollToView(By locator,String locatorName) throws HeadlessException, IOException, AWTException
	{
		boolean flag=false;
		{
			try {
				WebElement element = driver.findElement(locator);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(2000);
				Reporting.logResults("pass", "scroll to an element", "succesfully scrolled  to an  "+locatorName);
				return flag=true;
			}
			catch(Exception e)
			{
				Reporting.logResults("Fail", "scroll to an element","failed scrolled  to an  "+locatorName);
				return false;
			}
		}
		
		
	}
	
}


