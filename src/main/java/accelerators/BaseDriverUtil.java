package accelerators;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseDriverUtil {
	

	public static WebDriver driver;

	
	/**
	 * Launches the  browser based on the given values in the app.properties   
	 * 
	 * This method will launch the browser and URL based on the values provided in the properties file
	 * 
	 * @param browsername	specify  "chrome"  or "ie"
	 * @param url	specify www.google.com
	 * @author raghu
	 */
	
	public WebDriver launchBrowser(String browsername) 
	{
		if(browsername.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"/Drivers/geckodriver-v0.20.0-win64/geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			return driver;
		}

		if(browsername.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/Drivers/chromedriver_win32/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			return driver;
		}

		if(browsername.equalsIgnoreCase("opera"))
		{
			System.setProperty("webdriver.opera.driver", System.getProperty("user.dir")+"/Drivers/operadriver_win64/operadriver.exe");
			driver = new OperaDriver();
			driver.manage().window().maximize();
			return driver;
		}
		
		if(browsername.equalsIgnoreCase("edge"))
		{
			System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")+"/Drivers/MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
			driver.manage().window().maximize();
			return driver;
		}
		return null;

	}
	
	
	/**
	 * 
	 * THis method will read and stores data in the strings from properties file 
	 * @throws IOException
	 * @author raghu
	 */

	@BeforeMethod

	public void readDataFromProperty() throws IOException
	{
		try
		{
			ReadProperties rp = new ReadProperties();
			String browser = rp.readProperties().getProperty("browser");
			launchBrowser(browser);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * THis method will close the browser 
	 */

	@AfterMethod
	public void closeBrowser()
	{
		driver.close();
		driver.quit();
	}





}
